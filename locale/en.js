module.exports = {
    //general
    "password" : "Password",
    "forgotpassword" : "Forgot your Password?",
    "login": "LOGIN",
    "shopnow" : "SHOP NOW",
    "selectprice" : "Select Price:",
    "send" : "Send",

    //Menus
    "home" : "Home",
    "products" : "Products",

    //Top Menu
    "emptyCart": "Your Cart is Empty",
    "login": "Login",
    "join": "Join",
    "details": "Details",

    //Products
    "cartNItens" : 'Number of Itens',

    //cart
    "emptyCartMessage" : 'Your cart is empty, add itens to it',
    "ckeckout" : "Checkout",
    "discount" : "Discount",
    "tax": "Tax",
    "subTotal": "Sub-Total",

    //checkout
    "checkouTitle": "Checkout",
    "continueAsGuessTitle": "Continue as Guest",
    "checkoutGuestDescription" : "Continue as a guest and processed to payment to finish your purchase",
    "checkoutGuestContinueButton" : "Continue",
    "registeredcustomertitle" : "REGISTERED CUSTOMERS",
    "registeredcustomerdescription" : "If you have an account with us, please log in.",

    //footer
    "gethelp" : "GET HELP",
    "contactus": "Contact us",
    "shopping" : "Shopping",
    "orders" : "ORDERS",
    "paymentoption" : "Payment options",
    "shippingandelivery" : "Shipping and delivery",
    "returns" : "Returns",
    "register" : "REGISTER",
    "registerdescription": "Create one account to manage everything you do with Nike.",
    "developedby" : "Designed by",
    "termsofuse" : "Terms of Use",
    "privacycookiepolicy" : "Privacy & Cookie Policy",
    "cookiesettings" : "Cookie Settings",

    //error messages 
        //productReview
        "raterequired" : "Please choose the rate",

        //cart
        "cartmissingcolor" : "Must select the color",
        "cartmissingsize" : "Must select the size",
        "cartitemexist" : "This item with these features is already on the cart",

        //payment
        "wrongpaymentchoice" : "It is not possible to pay with this type of card on this currency",

    //product review
    "availablecolors" : "Available Colors",
    "availablesizes" : "Available Sizes",
    "instock" : "In stock",
    "quantity" : "Quantity",
    "price" : "Price",
    "addtocart" : "Add To Cart",

    //products
    "sort" : "Sort",
    "popular" : "Popular",
    "new": "New",
    "discount" : "Discount",
    "price" : "Price",
    "low" : "Low",
    "high" : "High",
    "laodmore" : "Load More",

    //CheckoutInfo
    "personalInfo": "Personal Information",
    "name" : "Name",
    "email" : "Email",
    "phone": "Phone",
    "address": "Address",
    "street" : "Street",
    "city" : "City",
    "country": "Country",
    "delivery" : "Delivery",
    "payment" : "Payment",
    "paymentdescription" : "Choose the payment method"
}