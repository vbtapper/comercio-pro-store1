module.exports = {
    //general
    "password" : "Senha",
    "forgotpassword" : "Esqueceu a Senha?",
    "login": "ENTRAR",
    "shopnow" : "COMPRAR AGORA",
    "selectprice" : "Selecionar Preço:",
    "send" : "Enviar",

    //Menus
    "home" : "Home",
    "products" : "Productos",

    //Top Menu
    "emptyCart": "Seu Carrinho Está Vázio",
    "login": "Entrar",
    "join": "Registrar",
    "details": "Detalhes",

    //Products
    "cartNItens" : 'Itens no carrinho',

    //cart
    "emptyCartMessage" : 'Seu carrinho está vazio, adiciona itens',
    "ckeckout" : "Pagamento",
    "discount" : "Desconto",
    "tax": "Taxa",
    "subTotal": "Sub-Total",

    //checkout
    "checkouTitle": "Ir Para Pagamento",
    "continueAsGuessTitle": "Continuar como Convidado",
    "checkoutGuestDescription" : "Continue como convidado e finalize a sua compra",
    "checkoutGuestContinueButton" : "Continuar",
    "registeredcustomertitle" : "CLIENTES REGISTRADOS",
    "registeredcustomerdescription" : "Se já tem uma conta por favor, insira suas credencias",

    //footer
    "gethelp" : "AJUDA",
    "contactus": "Contacta-nos",
    "shopping" : "Comparar",
    "orders" : "Pedidos",
    "paymentoption" : "Opcões de Pagamento",
    "shippingandelivery" : "Entrega e Envio",
    "returns" : "Devoluções",
    "register" : "REGISTRO",
    "registerdescription": "Cria uma conta e controla todo que compra aqui.",
    "developedby" : "Desenvolvido Por",
    "termsofuse" : "Termos de Uso",
    "privacycookiepolicy" : "Politicas de Privacidade e Cookie",
    "cookiesettings" : "Definições de Cookies",

    //error messages 
        //productReview
        "raterequired" : "Por favor escolha o nivel",

        //cart
        "cartmissingcolor" : "Deve selecionar a cor",
        "cartmissingsize" : "Deve selecionar o tamanho",
        "cartitemexist" : "Este item com essas caracteristicas já foi adicionado ao carrinho",

        //payment
        "wrongpaymentchoice" : "Não possivel pagar com este tipo de cartão nesta moeda",

    //product review
    "availablecolors" : "Cores Disponíveis",
    "availablesizes" : "Tamanhos Disponíveis",
    "instock" : "Em estoque",
    "quantity" : "Quantidade",
    "price" : "Preço",
    "addtocart" : "Adicionar ao Carrinho",

    //products
    "sort" : "Ordenar",
    "popular" : "Popular",
    "new": "Novo",
    "discount" : "Desconto",
    "price" : "Preço",
    "low" : "Baixo",
    "high" : "Alto",
    "laodmore" : "Carregar Mais",

    //CheckoutInfo
    "personalInfo": "Informações Pessoais",
    "name" : "Nome",
    "email" : "Email",
    "phone": "Telefone",
    "address": "Endereço",
    "street" : "Rua",
    "city" : "Cidade",
    "country": "Pais",
    "delivery" : "Entrega",
    "payment" : "Pagamento",
    "paymentdescription" : "Escolha o tipo de pagamento"

}