const IndexController                       = require('./Controllers/IndexController');
const ProductsController                    = require('./Controllers/ProductsController');
const CartController                        = require('./Controllers/CartController');
const CheckoutController                    = require('./Controllers/CheckoutController');
const ClientController                      = require('./Controllers/ClientController');
const PaymentController                     = require('./Controllers/PaymentController');

var Joi                                     = require('joi');

module.exports = [{
        method: 'GET',
        path: '/{param*}',
        handler: {
            directory: {
                path: 'assets',
                redirectToSlash: false,
                index: false,
                listing: false
            }
        }
    },

    {
        method: 'GET',
        path: '/products/{param*}',
        handler: {
            directory: {
                path: 'assets',
                redirectToSlash: false,
                index: false,
                listing: false
            }
        }
    },

    {
        method: 'GET',
        path: '/',
        handler: IndexController.RenderIndexPage
    },

    {
        method: 'GET',
        path: '/changelocale',
        config: {
            validate: {
                query: {
                    locale: Joi.string().required()
                }
            },
            handler: IndexController.ChangeLocale
        }
    },

    {
        method: 'GET',
        path: '/products',
        handler: ProductsController.RenderProductsPage
    },

    {
        method: 'GET',
        path: '/products/increaselimit',
        handler: ProductsController.IncreaseLimitDisplay
    },

    {
        method: 'GET',
        path: '/products/{prdID}',
        handler: ProductsController.RenderProductDetails
    },

    {
        method: 'POST',
        path: '/products/selectcolor',
        config: {
            validate: {
                payload: {
                    colorID: Joi.string().required(),
                    colorValue: Joi.string().required()
                }
            },
            handler: ProductsController.SelectProductColorToBuy
        }
    },

    {
        method: 'POST',
        path: '/products/selectsize',
        config: {
            validate: {
                payload: {
                    sizeID: Joi.string().required(),
                    sizeValue: Joi.number().required()
                }
            },
            handler: ProductsController.SelectProductSizeToBuy
        }
    },

    {
        method: 'POST',
        path: '/products/addtocart',
        config: {
            validate: {
                payload: {
                    quantity: Joi.number().required()
                }
            },
            handler: ProductsController.AddToCart
        }
    },

    {
        method: 'GET',
        path: '/cart',
        handler: CartController.RenderCartPage
    },

    {
        method: 'POST',
        path: '/cart/remove',
        config: {
            validate: {
                payload: {
                    productID: Joi.string().required()
                }
            },
            handler: CartController.RemoveFromCart
        }
    },

    {
        method: 'POST',
        path: '/cart/increaseQt',
        config: {
            validate: {
                payload: {
                    productID: Joi.string().required()
                }
            },
            handler: CartController.IncreaseQuantityProductCart
        }
    },

    {
        method: 'POST',
        path: '/cart/descreaseQt',
        config: {
            validate: {
                payload: {
                    productID: Joi.string().required()
                }
            },
            handler: CartController.DecreaseQuantityProductCart
        }
    },

    {
        method: 'GET',
        path: '/checkout',
        handler: CheckoutController.RenderChekoutPage
    },

    {
        method: 'POST',
        path: '/cart/enablecheckout',
        handler: CartController.EnableCheckout
    },

    {
        method: 'GET',
        path: '/join',
        handler: ClientController.RenderJoinPage
    },

    {
        method: 'GET',
        path: '/login',
        handler: ClientController.RenderLoginPage
    },

    {
        method: 'POST',
        path: '/join/register',
        config: {
            validate: {
                payload: {
                    firstname: Joi.string().required(),
                    lastname: Joi.string().required(),
                    email: Joi.string().required(),
                    password: Joi.string().required()
                }
            },
            handler: ClientController.RegisterClient
        }
    },

    {
        method: 'POST',
        path: '/login/action',
        config: {
            validate: {
                payload: {
                    email: Joi.string().required(),
                    password: Joi.string().required()
                }
            },
            handler: ClientController.LoginClient
        }
    },

    {
        method: 'GET',
        path: '/profile',
        handler: ClientController.RenderClientProfile
    },

    {
        method: 'POST',
        path: '/profile/addclientaddress',
        config: {
            validate: {
                payload: {
                    street: Joi.string().required(),
                    city: Joi.string().required(),
                    country: Joi.string().required()
                }
            },
            handler: ClientController.AddClientAddress
        }
    },

    {
        method: 'GET',
        path: '/logout',
        handler: ClientController.LogOut
    },

    {
        method: 'GET',
        path: '/ckeckoutinfo',
        handler: CheckoutController.RenderCkeckoutInformation 
    },

    {
        method: 'GET',
        path: '/asguess',
        handler: CheckoutController.ContinueAsGuess,
    },

    {
        method: 'POST',
        path: '/ckeckout/asclient',
        config: {
            validate: {
                payload: {
                    email: Joi.string().email().required(),
                    password: Joi.string().required()
                }
            },
            handler: CheckoutController.ContinueAsClient
        }
    },

    {
        method: 'POST',
        path: '/payment/ekumbu',
        config: {
            validate: {
                payload: {
                    cardN: Joi.string().required(),
                    expDate: Joi.string().required(),
                    customerName: Joi.string().required(),
                    customerPhone: Joi.string().required(),
                    customerEmail: Joi.string().required(),
                    addressStreet: Joi.string().required(),
                    addressCity: Joi.string().required(),
                    addressCountry: Joi.string().required()
                }
            },
            handler: PaymentController.ProcessEkumbuPayment
        }
    },

    {
        method: 'POST',
        path: '/payment/visa',
        config: {
            validate: {
                payload: {
                    cardN: Joi.string().required(),
                    expDate: Joi.string().required(),
                    ccv: Joi.number(),
                    customerName: Joi.string().required(),
                    customerPhone: Joi.string().required(),
                    customerEmail: Joi.string().required(),
                    addressStreet: Joi.string().required(),
                    addressCity: Joi.string().required(),
                    addressCountry: Joi.string().required()
                }
            },
            handler: PaymentController.ProcessStripePayment
        }
    },

    {
        method: 'GET',
        path: '/successpurchase',
        handler: CheckoutController.RenderSuccessPage
    },

    {
        method: 'PATCH',
        path: '/products/addreview',
        config: {
            validate: {
                payload: {
                    rate: Joi.number().required(),
                    email: Joi.string().email().required(),
                    name: Joi.string().required(),
                    comment: Joi.string().required()
                }
            },
            handler: ProductsController.AddProductReview
        }
    },

    {
        method: 'POST',
        path: '/choosepaymenttype',
        config: {
            validate: {
                payload: {
                    type: Joi.string().required(),
                },
            },
            handler: CheckoutController.ChoosePaymentType
        }

    }
]

