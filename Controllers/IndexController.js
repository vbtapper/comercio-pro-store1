
var enLocale                = require('../locale/en.js');
var ptLocale                = require('../locale/pt.js');
const Client                = require('node-rest-client').Client;
const Q                     = require('q');
const async                 = require("async");
const randtoken             = require('rand-token').generator();

const config                = require('../config.js');

function GetRate(fromParam) {
    var deferred = Q.defer();
    var client = new Client();

    var args = {
        headers: { 
            "Content-Type": "application/json",
        },
        parameters: { 
            from: fromParam,
        }
    };

    client.get(config.exchangeRate.devEndpoint + "/kwanzaer/getrate", args, function (data, response) {
        if(data['statusCode'] == 200) {
            deferred.resolve(data)
        }
        else {
            deferred.reject(data)
        }   
    });
    return deferred.promise;   
}

function GetProductsMainSlider() {
    var deferred = Q.defer();
    var client = new Client();

    var attributesArray=['slider'];

    var args = {
        headers: { 
            "Content-Type": "application/json",
            "Authorization" : config.API.accessKey
        },
        data: { 
            limit: 3,
            attributes: attributesArray,
        }
    }

    client.post(config.API.endpointdev + "/client/getproductssellbyattributes", args, function (data, response) {
        if(data['statusCode'] == 200) {
            deferred.resolve(data)
        }
        else {
            deferred.reject(data)
        }   
    });
    return deferred.promise;   
}

function GetProductsSideBar() {
    var deferred = Q.defer();
    var client = new Client();

    var attributesArray=['sidebar'];

    var args = {
        headers: { 
            "Content-Type": "application/json",
            "Authorization" : config.API.accessKey
        },
        data: { 
            limit: 3,
            attributes: attributesArray,
        }
    }

    client.post(config.API.endpointdev + "/client/getproductssellbyattributes", args, function (data, response) {
        if(data['statusCode'] == 200) {
            deferred.resolve(data)
        }
        else {
            deferred.reject(data)
        }   
    });
    return deferred.promise; 
}

function GetProductsContent() {
    var deferred = Q.defer();
    var client = new Client();

    var args = {
        headers: { 
            "Content-Type": "application/json",
            "Authorization" : config.API.accessKey
        },
        parameters: { 
            limit: 6,
        }
    };

    client.get(config.API.endpointdev + "/client/getproductssell", args, function (data, response) {
        if(data['statusCode'] == 200) {
            deferred.resolve(data)
        }
        else {
            deferred.reject(data)
        }   
    });
    return deferred.promise;   
}

module.exports = {
    RenderIndexPage: function(request, reply) {
        
        var arrLanguage;
    
        if(request.session.locate === undefined) {
            request.session.locate = 'pt'
            arrLanguage = ptLocale
            request.session.currency = 'kz'
        }  
        else {
            if(request.session.locate === 'pt') {
                request.session.locate = 'pt'
                arrLanguage = ptLocale
                request.session.currency = 'kz'
            }
            else {
                request.session.locate = 'en'
                arrLanguage = enLocale;
                request.session.currency = '$'
            }
        }     

        if(request.session.cart === undefined) {
            request.session.cart = [];
        }

        request.session.checkout = undefined;
        request.session.initLimitProducts = 6;

        GetProductsMainSlider()
        .then(FoundProducts => {
            GetProductsContent()
            .then(ProductsContent => {
                GetProductsSideBar()
                .then(FoundProductsSideBar => {
                    GetRate('USD')
                    .then(FoundRate => {
                        var objData = {
                            language: arrLanguage,
                            locate: request.session.locate,
                            slideProducts: FoundProducts.products,
                            contentProducts: ProductsContent.products,
                            currency: request.session.currency,
                            cartNItens: request.session.cart.length,
                            rate: FoundRate.rate,
                            sidebar: FoundProductsSideBar.products
                        }
                        return reply.view('index', objData, { layout: 'default' });
                    }) 
                })
                .catch(error => {
                    console.log(error)
                    return reply.view(new Error('500'));
                }) 
            })
            .catch(error => {
                console.log(error)
                return reply.view(new Error('500'));
            })
        })
        .catch(error => {
            console.log(error)
            return reply.view(new Error('500'));
        })
    },

    ChangeLocale: function(request, reply) {
        request.session.locate = request.query.locale;
        var infoResponse = {
            statusCode: 200,
            error: null,
            message: '/'
        }
        return reply(infoResponse)
    }
}