var enLocale                = require('../locale/en.js');
var ptLocale                = require('../locale/pt.js');
const Client                = require('node-rest-client').Client;
const Q                     = require('q');
const async                 = require("async");
const randtoken             = require('rand-token').generator();

const config                = require('../config.js');

function GetRate(fromParam) {
    var deferred = Q.defer();
    var client = new Client();

    var args = {
        headers: { 
            "Content-Type": "application/json",
        },
        parameters: { 
            from: fromParam,
        }
    };

    client.get(config.exchangeRate.devEndpoint + "/kwanzaer/getrate", args, function (data, response) {
        if(data['statusCode'] == 200) {
            deferred.resolve(data)
        }
        else {
            deferred.reject(data)
        }   
    });
    return deferred.promise;   
}

module.exports = {
    RenderCartPage: function(request, reply) {

        var arrLanguage;
       
        if(request.session.locate === undefined) {
            request.session.locate = 'pt'
            arrLanguage = ptLocale
            request.session.currency = 'kz'
        }  
        else {
            if(request.session.locate === 'pt') {
                request.session.locate = 'pt'
                arrLanguage = ptLocale
                request.session.currency = 'kz'
            }
            else {
                request.session.locate = 'en'
                arrLanguage = enLocale;
                request.session.currency = '$'
            }
        }

        request.session.checkout = undefined;

        cartTotal = 0;
        async.forEach(request.session.cart, function (element, callback){ 
            cartTotal = cartTotal + (element.prodPrice * element.quantity)
            callback()
        }, function(err) {
            GetRate('USD')
            .then(FoundRate => {
                var objData = {
                    language: arrLanguage,
                    locate: request.session.locate,
                    currency: request.session.currency,
                    cartNItens: request.session.cart.length,
                    cart: request.session.cart,
                    cartTotal: cartTotal,
                    rate: FoundRate.rate
                }
                return reply.view('cart', objData, { layout: 'default' });
            })
        });
        
    },

    RemoveFromCart: function(request, reply) {
        request.session.cart = request.session.cart.filter(obj => obj.productPublicID !== request.payload.productID);
        var infoResponse = {
            statusCode: 200,
            error: null,
            message: 'cart'
        }
        return reply(infoResponse)
    },

    IncreaseQuantityProductCart: function(request, reply) {
        var index = 0;
        async.forEach(request.session.cart, function (element, callback){ 
            if(element.productPublicID === request.payload.productID) {
                request.session.cart[index].quantity = request.session.cart[index].quantity + 1;
            }
            index = index + 1;
            callback()
        }, function(err) {
            var infoResponse = {
                statusCode: 200,
                error: null,
                message: 'cart'
            }
            return reply(infoResponse)
        });     
    },

    DecreaseQuantityProductCart: function(request, reply) {
        var itemSelectd = request.session.cart.filter(obj => obj.productPublicID === request.payload.productID)[0];
        if(itemSelectd.quantity === 1) {
            request.session.cart = request.session.cart.filter(obj => obj.productPublicID !== request.payload.productID);
            var infoResponse = {
                statusCode: 200,
                error: null,
                message: 'cart'
            }
            return reply(infoResponse)
        }
        else {
            var index = 0;
            async.forEach(request.session.cart, function (element, callback){ 
                if(element.productPublicID === request.payload.productID) {
                    request.session.cart[index].quantity = request.session.cart[index].quantity - 1;
                }
                index = index + 1;
                callback()
            }, function(err) {
                var infoResponse = {
                    statusCode: 200,
                    error: null,
                    message: 'cart'
                }
                return reply(infoResponse)
            });
            
        }
    },

    EnableCheckout: function(request, reply) {
        request.session.checkout = true;
        var infoResponse = {
            statusCode: 200,
            error: null,
            message: 'checkout'
        }
        return reply(infoResponse)
    }

}