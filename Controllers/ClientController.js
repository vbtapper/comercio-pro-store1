var enLocale                = require('../locale/en.js');
var ptLocale                = require('../locale/pt.js');
const Client                = require('node-rest-client').Client;
const Q                     = require('q');
const async                 = require("async");
const randtoken             = require('rand-token').generator();

const config                = require('../config.js');

function isEmptyObject(obj) {
  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      return false;
    }
  }
  return true;
}

function RegisterClientRequest(newClientObj) {
    var deferred = Q.defer();
    var client = new Client();

    var args = {
        headers: { 
            "Content-Type": "application/json",
            "Authorization" : config.API.accessKey
        },
        data: { 
            email: newClientObj.email,
            firstname: newClientObj.firstname,
            lastname: newClientObj.lastname,
            password: newClientObj.password,
            address: newClientObj.address
        }
    };

    client.post(config.API.endpointdev + "/client/addclient", args, function (data, response) {
        if(data['statusCode'] == 200) {
            deferred.resolve(data)
        }
        else {
            deferred.reject(data)
        }   
    });
    return deferred.promise;   
}

function LoginClientRequest(loginObj) {
    var deferred = Q.defer();
    var client = new Client();

    var args = {
        headers: { 
            "Content-Type": "application/json",
            "Authorization" : config.API.accessKey
        },
        parameters: { 
            email: loginObj.email,
            password: loginObj.password,
        }
    };

    client.get(config.API.endpointdev + "/client/login", args, function (data, response) {
        if(data['statusCode'] == 200) {
            deferred.resolve(data)
        }
        else {
            deferred.reject(data)
        }   
    });
    return deferred.promise;   
}

function AddClientAddressRequest(newAddressObj) {
    var deferred = Q.defer();
    var client = new Client();

    var args = {
        headers: { 
            "Content-Type": "application/json",
            "Authorization" : config.API.accessKey
        },
        data: { 
            email: newAddressObj.email,
            street: newAddressObj.street,
            city: newAddressObj.city,
            country: newAddressObj.country
        }
    };

    client.post(config.API.endpointdev + "/client/addaddress", args, function (data, response) {
        if(data['statusCode'] == 200) {
            deferred.resolve(data)
        }
        else {
            deferred.reject(data)
        }   
    });
    return deferred.promise;   
}

module.exports = {
    RenderJoinPage: function(request, reply) {
        var arrLanguage = ptLocale;
        var localeParam = 'pt';
        request.session.currency = 'AOA'
        
        if(request.plugins.getLocales.toString() === 'en_us') {
            arrLanguage = enLocale
            request.session.currency = '$'
            localeParam = 'en';
        }       

        var objData = {
            language: arrLanguage,
            locate: localeParam,
            currency: request.session.currency,
        }
        return reply.view('join', objData, { layout: 'auth' });
        
    },

    RenderLoginPage: function(request, reply) {
        var arrLanguage = ptLocale;
        var localeParam = 'pt';
        request.session.currency = 'AOA'
        
        if(request.plugins.getLocales.toString() === 'en_us') {
            arrLanguage = enLocale
            request.session.currency = '$'
            localeParam = 'en';
        }       

        var objData = {
            language: arrLanguage,
            locate: localeParam,
            currency: request.session.currency,
        }
        return reply.view('login', objData, { layout: 'auth' });
    },

    RenderClientProfile: function(request, reply) {
        var arrLanguage = ptLocale;
        var localeParam = 'pt';
        request.session.currency = 'AOA'
        
        if(request.plugins.getLocales.toString() === 'en_us') {
            arrLanguage = enLocale
            request.session.currency = '$'
            localeParam = 'en';
        }       

        if(request.session.user === undefined) {
            return reply.redirect('/', {}, { layout: 'default' });  
        }
        else {
            
            var objData = {
                language: arrLanguage,
                locate: localeParam,
                currency: request.session.currency,
                addresses: request.session.addresses,
                naddresses: request.session.naddresses
            }
            return reply.view('profile', objData, { layout: 'profile' }); 
        }
          
    },

    RegisterClient: function(request, reply) {
        var nwClObj = {
            email: request.payload.email,
            firstname: request.payload.firstname,
            lastname: request.payload.lastname,
            password: request.payload.password,
            address: []
        }
        RegisterClientRequest(nwClObj)
        .then(newClientObjResponse => {
            request.session.user = request.payload.email;
            var infoResponse = {
                statusCode: 200,
                error: null,
                message: 'profile'
            }
            return reply(infoResponse)
        })
        .catch(error => {
            console.log(error);
            var infoResponse = {
                statusCode: 500,
                error: 'Internal Error',
                message: 'Ocorreu um erro interno, por favor tente mais tarde'
            }
            return reply(infoResponse)
        })
    },

    LoginClient: function(request, reply) {
        var nwClObj = {
            email: request.payload.email,
            password: request.payload.password,
        }

        LoginClientRequest(nwClObj)
        .then(newClientObjResponse => {
            request.session.user = request.payload.email;
            request.session.addresses = newClientObjResponse.addresses;
            request.session.naddresses = newClientObjResponse.addresses.length;

            var infoResponse = {
                statusCode: 200,
                error: null,
                message: 'profile'
            }
            return reply(infoResponse)
        })
        .catch(error => {
            console.log(error);
            return reply(error);
        })
    },

    AddClientAddress: function(request, reply) {
        var newAddrObj = {
            email: request.session.user,
            street: request.payload.street,
            city: request.payload.city,
            country: request.payload.country
        }

        AddClientAddressRequest(newAddrObj)
        .then(SuccessResponse => {
            request.session.addresses = SuccessResponse['addresses']
            request.session.naddresses = SuccessResponse['addresses'].length;
            var infoResponse = {
                statusCode: 200,
                error: null,
                message: 'profile'
            }
            return reply(infoResponse)
        })
        .catch(error => {
            console.log(error);
            return reply(error);
        })
    },

    LogOut: function(request, reply) {
        request.session.user = undefined;
        request.session.addresses = undefined;
        request.session.naddresses = undefined;

        return reply.redirect('/', {}, { layout: 'default' });  
    }
}