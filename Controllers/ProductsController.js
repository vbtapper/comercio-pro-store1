var enLocale                = require('../locale/en.js');
var ptLocale                = require('../locale/pt.js');
const Client                = require('node-rest-client').Client;
const Q                     = require('q');
const async                 = require("async");
const randtoken             = require('rand-token').generator();

const config                = require('../config.js');
var numberOfRecords = 0;
var SelectedProduct = null;
var SelectedColor = null;
var SelectedSize = null;
var cartObj = [];

function GetRate(fromParam) {
    var deferred = Q.defer();
    var client = new Client();

    var args = {
        headers: { 
            "Content-Type": "application/json",
        },
        parameters: { 
            from: fromParam,
        }
    };

    client.get(config.exchangeRate.devEndpoint + "/kwanzaer/getrate", args, function (data, response) {
        if(data['statusCode'] == 200) {
            deferred.resolve(data)
        }
        else {
            deferred.reject(data)
        }   
    });
    return deferred.promise;   
}

function GetProductsContent(limitParam) {
    
    var deferred = Q.defer();
    var client = new Client();

    var args = {
        headers: { 
            "Content-Type": "application/json",
            "Authorization" : config.API.accessKey
        },
        parameters: { 
            limit: limitParam,
        }
    };

    client.get(config.API.endpointdev + "/client/getproductssell", args, function (data, response) {
        if(data['statusCode'] == 200) {
            deferred.resolve(data)
        }
        else {
            deferred.reject(data)
        }   
    });
    return deferred.promise;   
}

function GetProductsByModel(modelParam) {
    
    var deferred = Q.defer();
    var client = new Client();

    var args = {
        headers: { 
            "Content-Type": "application/json",
            "Authorization" : config.API.accessKey
        },
        parameters: { 
            model: modelParam,
        }
    };

    client.get(config.API.endpointdev + "/client/getproductsbymodel", args, function (data, response) {
        if(data['statusCode'] == 200) {
            deferred.resolve(data)
        }
        else {
            deferred.reject(data)
        }   
    });
    return deferred.promise;   
}

function GetProductDetail(productIDParam) {
    var deferred = Q.defer();
    var client = new Client();

    var args = {
        headers: { 
            "Content-Type": "application/json",
            "Authorization" : config.API.accessKey
        },
        parameters: { 
            productID: productIDParam,
        }
    };

    client.get(config.API.endpointdev + "/client/getproductdetails", args, function (data, response) {
        if(data['statusCode'] == 200) {
            deferred.resolve(data)
        }
        else {
            deferred.reject(data)
        }   
    });
    return deferred.promise;   
}

function GetAttributesByKey(productIDParam, attributeIDParam) {
    var deferred = Q.defer();
    var client = new Client();

    var args = {
        headers: { 
            "Content-Type": "application/json",
            "Authorization" : config.API.accessKey
        },
        parameters: { 
            attributeID: attributeIDParam,
            productID: productIDParam,
        }
    };

    client.get(config.API.endpointdev + "/client/getattributesbykey", args, function (data, response) {
        if(data['statusCode'] == 200) {
            deferred.resolve(data)
        }
        else {
            deferred.reject(data)
        }   
    });
    return deferred.promise;   
}

function AddReviewRequest(newReviewObj) {
    
    var deferred = Q.defer();
    var client = new Client();

    var args = {
        headers: { 
            "Content-Type": "application/json",
            "Authorization" : config.API.accessKey
        },
        data: { 
            rate: newReviewObj.rate,
            email: newReviewObj.email,
            name: newReviewObj.name,
            message: newReviewObj.message,
            productID: SelectedProduct.publicID
        }
    };

    client.patch(config.API.endpointdev + "/client/addreview", args, function (data, response) {
        if(data['statusCode'] == 200) {
            deferred.resolve(data)
        }
        else {
            deferred.reject(data)
        }   
    });
    return deferred.promise;   
}

module.exports = {

    RenderProductsPage: function(request, reply) {
        
        var arrLanguage;
        var rateParam;

        if(request.session.locate === undefined) {
            request.session.locate = 'pt'
            arrLanguage = ptLocale
            request.session.currency = 'kz'
        }  
        else {
            if(request.session.locate === 'pt') {
                request.session.locate = 'pt'
                arrLanguage = ptLocale
                request.session.currency = 'kz'
            }
            else {
                request.session.locate = 'en'
                arrLanguage = enLocale;
                request.session.currency = '$'
            }
        }      

        if(request.session.cart === undefined) {
            request.session.cart = [];
        }

        if(request.session.initLimitProducts === undefined) {
            request.session.initLimitProducts = 6;
        }

        request.session.checkout = undefined;
        GetProductsContent(request.session.initLimitProducts)
        .then(ProductsContent => {
            GetRate('USD')
            .then(FoundRate => {
                
                var objData = {
                    language: arrLanguage,
                    locate: request.session.locate,
                    contentProducts: ProductsContent.products,
                    nrecords: ProductsContent.numberOfRecords,
                    currency: request.session.currency,
                    cartNItens: request.session.cart.length,
                    rate: FoundRate.rate
                }
                numberOfRecords = ProductsContent.numberOfRecords;
                return reply.view('products', objData, { layout: 'default' });
            })

            
        })
        .catch(error => {
            console.log(error)
            return reply.view(new Error('500'));
        })
    },

    RenderProductDetails: function(request, reply) {
        
        SelectedColor = null;
        SelectedSize = null;
        SelectedProduct = null;
        var colorArray = [];
        var sizeArray = [];

        var arrLanguage;
        
        if(request.session.locate === undefined) {
            request.session.locate = 'pt'
            arrLanguage = ptLocale
            request.session.currency = 'kz'
        }  
        else {
            if(request.session.locate === 'pt') {
                request.session.locate = 'pt'
                arrLanguage = ptLocale
                request.session.currency = 'kz'
            }
            else {
                request.session.locate = 'en'
                arrLanguage = enLocale;
                request.session.currency = '$'
            }
        }  

        if(request.session.cart === undefined) {
            request.session.cart = [];
        }

        request.session.checkout = undefined;
        
        GetProductDetail(request.params.prdID)
        .then(FoundProduct => {
            SelectedProduct = FoundProduct.product;
            async.forEach(FoundProduct.product.attributes, function (element, callback){ 
                if(element.type === 'color' && element.quantity > 0) {
                    colorArray.push({
                        colorID: element.id,
                        colorValue: element.value
                    })
                }
                
                if(element.type === 'size' && element.quantity > 0) {
                    var notInThere = true
                    sizeArray.forEach(function(elm) {
                        if(elm.sizeValue === element.value) {
                            notInThere = false;
                        }
                    })
                    if (notInThere) {
                        sizeArray.push({
                            sizeID: element.id,
                            sizeValue: element.value
                        })
                    }
                }

                callback();
            }, function(err) {
                GetProductsByModel(SelectedProduct.data.model)
                .then(FoundProductsByModel => {
                    GetRate('USD')
                    .then(FoundRate => {
                        var objData = {
                            language: arrLanguage,
                            locate: request.session.locate,
                            product: FoundProduct.product,
                            currency: request.session.currency,
                            cartNItens: request.session.cart.length,
                            colors: colorArray,
                            sizes: sizeArray.sort(),
                            rate: FoundRate.rate,
                            otherproducts: FoundProductsByModel.products
                        }
                        return reply.view('details', objData, { layout: 'default' });
                    })
                })
                .catch(error => {
                    console.log(error)
                    return reply.view(new Error('500'));
                })
            });
        })
        .catch(error => {
            console.log(error)
            return reply.view(new Error('400'));
        })
    },

    IncreaseLimitDisplay: function(request, reply) {
        if(numberOfRecords >= request.session.initLimitProducts) {
            request.session.initLimitProducts = request.session.initLimitProducts + 4;
            var infoResponse = {
                statusCode: 200,
                error: null,
                message: 'products'
            }
            return reply(infoResponse)
        }
        else {
            var infoResponse = {
                statusCode: 403,
                error: 'Request Error',
                message: 'limit reached'
            }
            return reply(infoResponse)
        }
        
    },

    SelectProductColorToBuy: function(request, reply) {
        GetAttributesByKey(SelectedProduct.publicID, request.payload.colorID)
        .then(FoundAttributes => {

            SelectedColor = {
                id: request.payload.colorID,
                value: request.payload.colorValue
            } 

            var SizesArray = FoundAttributes.attributes.filter(obj => obj.type === 'size' && obj.quantity > 0);
            var infoResponse = {
                statusCode: 200,
                error: null,
                message: 'success',
                array: SizesArray.sort()
            }
            return reply(infoResponse)
        })
        .catch(error => {
            console.log(error)
            var infoResponse = {
                statusCode: 500,
                error: 'Internal Error',
                message: 'Ocorreu um erro interno. Por favor tente mais tarde'
            }
            return reply(infoResponse)
        })
    },

    SelectProductSizeToBuy: function(request, reply) {
        if(SelectedColor === null) {
            var infoResponse = {
                statusCode: 403,
                error: 'Request Error',
                message: 'Deve selecionar a cor primeiro'
            }
            return reply(infoResponse)
        }
        else {
            SelectedSize = {
                id: request.payload.sizeID,
                value: request.payload.sizeValue
            } 

            var infoResponse = {
                statusCode: 200,
                error: null,
                message: 'success'
            }
            return reply(infoResponse)
        }
    },

    AddToCart: function(request, reply) {
        
        var locateFile = null;
        if(request.session.locate === 'pt') {
            locateFile = ptLocale
        }
        else {
            locateFile = enLocale
        }

        if(SelectedColor === null) {
            var infoResponse = {
                statusCode: 403,
                error: 'Request Error',
                message: locateFile['cartmissingcolor']
            } 
            return reply(infoResponse)
        }
        else if(SelectedSize === null) {
            var infoResponse = {
                statusCode: 403,
                error: 'Request Error',
                message: locateFile['cartmissingsize']
            } 
            return reply(infoResponse)
        }
        else {

            var notInThere = false
            request.session.cart.forEach(function(elm) {
                if(elm.productPublicID === SelectedProduct.publicID) {
                    notInThere = true;
                }
            })

            if(notInThere) {
                var infoResponse = {
                    statusCode: 403,
                    error: 'Request Error',
                    message: locateFile['cartitemexist']
                }
                return reply(infoResponse)
            }
            else {
                var cartObject = {
                    productPublicID: SelectedProduct.publicID,
                    prodName: SelectedProduct.general.productName,
                    prodKeywords: SelectedProduct.general.metaTagKeywords,
                    prodPrice: SelectedProduct.sellingPrice,
                    quantity: request.payload.quantity,
                    attributes: {
                        color: {
                            id: SelectedColor.id,
                            value: SelectedColor.value
                        },
                        size: {
                            id: SelectedSize.id,
                            value: SelectedSize.value
                        }
                    },
                    picture: SelectedProduct.images[0].path
                };

                request.session.cart.push(cartObject)
                var nitensParams = cartObj.length;

                var infoResponse = {
                    statusCode: 200,
                    error: null,
                    message: 'success',
                    nitens: request.session.cart.length,
                    cartmessage: locateFile['cartNItens']
                }
                return reply(infoResponse)
            }
        }
    },

    AddProductReview: function(request, reply) {

        var newReview = {
            rate: request.payload.rate,
            email: request.payload.email,
            name: request.payload.name,
            message: request.payload.comment,
        }
        AddReviewRequest(newReview)
        .then(NewReviewResponse => {
            var infoResponse = {
                statusCode: 200,
                error: null,
                message: '/products/' + SelectedProduct.publicID
            }
            return reply(infoResponse);
        })
        .catch(error => {
            console.log(error)
            return reply(error)
        })
    }
}