var enLocale                = require('../locale/en.js');
var ptLocale                = require('../locale/pt.js');
const Client                = require('node-rest-client').Client;
const Q                     = require('q');
const async                 = require("async");
const randtoken             = require('rand-token').generator();

const config                = require('../config.js');

function isEmptyObject(obj) {
  for (var key in obj) {
    if (Object.prototype.hasOwnProperty.call(obj, key)) {
      return false;
    }
  }
  return true;
}

function LoginClientRequest(loginObj) {
    var deferred = Q.defer();
    var client = new Client();

    var args = {
        headers: { 
            "Content-Type": "application/json",
            "Authorization" : config.API.accessKey
        },
        parameters: { 
            email: loginObj.email,
            password: loginObj.password,
        }
    };

    client.get(config.API.endpointdev + "/client/login", args, function (data, response) {
        if(data['statusCode'] == 200) {
            deferred.resolve(data)
        }
        else {
            deferred.reject(data)
        }   
    });
    return deferred.promise;   
}

function GetRate(fromParam) {
    var deferred = Q.defer();
    var client = new Client();

    var args = {
        headers: { 
            "Content-Type": "application/json",
        },
        parameters: { 
            from: fromParam,
        }
    };

    client.get(config.exchangeRate.devEndpoint + "/kwanzaer/getrate", args, function (data, response) {
        if(data['statusCode'] == 200) {
            deferred.resolve(data)
        }
        else {
            deferred.reject(data)
        }   
    });
    return deferred.promise;   
}

function LoginClientRequest(loginObj) {
    var deferred = Q.defer();
    var client = new Client();

    var args = {
        headers: { 
            "Content-Type": "application/json",
            "Authorization" : config.API.accessKey
        },
        parameters: { 
            email: loginObj.email,
            password: loginObj.password,
        }
    };

    client.get(config.API.endpointdev + "/client/login", args, function (data, response) {
        if(data['statusCode'] == 200) {
            deferred.resolve(data)
        }
        else {
            deferred.reject(data)
        }   
    });
    return deferred.promise;   
}

module.exports = {
    RenderChekoutPage: function(request, reply) {
        var arrLanguage;
     
        request.session.checkoutuser = undefined;
        request.session.checkoutname = undefined;
        request.session.checkoutaddresses = undefined;
        request.session.checkoutinfo = undefined;

        if(request.session.locate === undefined) {
            request.session.locate = 'pt'
            arrLanguage = ptLocale
            request.session.currency = 'kz'
        }  
        else {
            if(request.session.locate === 'pt') {
                request.session.locate = 'pt'
                arrLanguage = ptLocale
                request.session.currency = 'kz'
            }
            else {
                request.session.locate = 'en'
                arrLanguage = enLocale;
                request.session.currency = '$'
            }
        }       

        if(request.session.checkout === undefined) {
            return reply.redirect('/', {}, { layout: 'default' });  
        }
        else {
            request.session.checkoutinfo = undefined;

            var objData = {
                language: arrLanguage,
                locate: request.session.locate,
                currency: request.session.currency,
            }
            return reply.view('checkout', objData, { layout: 'default' });
        }
    },

    ContinueAsGuess: function(request, reply) {
        request.session.checkoutinfo = "asguess";
        return reply.redirect('ckeckoutinfo', {}, { layout: 'default' });
    },

    ContinueAsClient: function(request, reply) {
        var nwClObj = {
            email: request.payload.email,
            password: request.payload.password,
        }

        LoginClientRequest(nwClObj)
        .then(newClientObjResponse => {
            request.session.checkoutuser = request.payload.email;
            request.session.checkoutname = newClientObjResponse.firstname + " " + newClientObjResponse.lastname;
            request.session.checkoutaddresses = newClientObjResponse.addresses[0];
            request.session.checkoutinfo = "asclient";

            var infoResponse = {
                statusCode: 200,
                error: null,
                message: 'ckeckoutinfo'
            }
            return reply(infoResponse)
        })
        .catch(error => {
            console.log(error);
            return reply(error);
        })
    },

    RenderCkeckoutInformation: function(request, reply) {
        var arrLanguage;
        
        if(request.session.locate === undefined) {
            request.session.locate = 'pt'
            arrLanguage = ptLocale
            request.session.currency = 'kz'
        }  
        else {
            if(request.session.locate === 'pt') {
                request.session.locate = 'pt'
                arrLanguage = ptLocale
                request.session.currency = 'kz'
            }
            else {
                request.session.locate = 'en'
                arrLanguage = enLocale;
                request.session.currency = '$'
            }
        }       

        if(request.session.checkoutinfo === undefined) {
            return reply.redirect('/', {}, { layout: 'default' });  
        }
        else {

            request.session.paymentdone = undefined;
            request.session.checkout = undefined;

            cartTotal = 0;
            async.forEach(request.session.cart, function (element, callback){ 
                cartTotal = cartTotal + (element.prodPrice * element.quantity)
                request.session.subtotal = cartTotal;
                callback()
            }, function(err) {
                GetRate('USD')
                .then(FoundRate => {
                    var objData = {
                        language: arrLanguage,
                        locate: request.session.locate,
                        currency: request.session.currency,
                        cartNItens: request.session.cart.length,
                        cart: request.session.cart,
                        cartTotal: cartTotal,
                        type: request.session.checkoutinfo,
                        clientname: request.session.checkoutname,
                        clientemail: request.session.checkoutuser,
                        clientaddress: request.session.checkoutaddresses,
                        rate: FoundRate.rate
                    }
                    return reply.view('checkoutinfo', objData, { layout: 'checkout' });
                })
            });
            
        }
    },

    RenderSuccessPage: function(request, reply) {
        
        request.session.checkoutinfo = undefined;
        var arrLanguage;
        
        if(request.session.locate === undefined) {
            request.session.locate = 'pt'
            arrLanguage = ptLocale
            request.session.currency = 'kz'
        }  
        else {
            if(request.session.locate === 'pt') {
                request.session.locate = 'pt'
                arrLanguage = ptLocale
                request.session.currency = 'kz'
            }
            else {
                request.session.locate = 'en'
                arrLanguage = enLocale;
                request.session.currency = '$'
            }
        }
        
        if(request.session.paymentdone === undefined) {
            return reply.redirect('/', {}, { layout: 'default' });  
        }
        else {
            var data = {
                language: arrLanguage,
                locate: request.session.locate,
                purchaseID: request.session.paymentID
            }
            request.session.paymentdone = undefined;
            request.session.paymentID = null;
            return reply.view('buysuccess', data, { layout: 'default' });   
        }
    },

    ChoosePaymentType: function(request, reply) {

        var locateFile = null;
        if(request.session.locate === 'pt') {
            locateFile = ptLocale
        }
        else {
            locateFile = enLocale
        }

        if(request.payload.type === 'ekumbu') {
            if(request.session.locate === 'pt') {
                var infoResponse = {
                    statusCode: 200,
                    error: null,
                    message: 'success'
                }
                return reply(infoResponse)
            }
            else {
                var infoResponse = {
                    statusCode: 403,
                    error: 'Request Error',
                    message: locateFile['wrongpaymentchoice']
                }
                return reply(infoResponse)
            }
        }
        else {
            if(request.session.locate === 'en') {
                var infoResponse = {
                    statusCode: 200,
                    error: null,
                    message: 'success'
                }
                return reply(infoResponse)
            }
            else {
                var infoResponse = {
                    statusCode: 403,
                    error: 'Request Error',
                    message: locateFile['wrongpaymentchoice']
                }
                return reply(infoResponse)
            }   
        }
    }
}