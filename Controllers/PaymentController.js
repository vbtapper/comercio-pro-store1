var Client                  = require('node-rest-client').Client;
var unirest                 = require('unirest');
const Q                     = require('q');
const config                = require('../config.js');

function GetRate(fromParam) {
    var deferred = Q.defer();
    var client = new Client();

    var args = {
        headers: { 
            "Content-Type": "application/json",
        },
        parameters: { 
            from: fromParam,
        }
    };

    client.get(config.exchangeRate.devEndpoint + "/kwanzaer/getrate", args, function (data, response) {
        if(data['statusCode'] == 200) {
            deferred.resolve(data)
        }
        else {
            deferred.reject(data)
        }   
    });
    return deferred.promise;   
}

function isEmptyObject(obj) {
    for (var key in obj) {
        if (Object.prototype.hasOwnProperty.call(obj, key)) {
            return false;
        }
    }
    return true;
}

function EkumubPaymentRequest(paymentObj) {
    var deferred = Q.defer();
    
    unirest.post(config.paymentGateway.devEndpoint + "/ekumbu/payment")
    .headers({'Accept': 'application/json', 'Content-Type': 'application/json'})
    .auth({user : "user", pass: "password", sendImmediately: true})
    .send({ 
        "cardNumber": paymentObj.cardNumber, 
        "expMonth": paymentObj.cardMonth,
        "expYear": paymentObj.cardYear,
        "customerIP" : paymentObj.ipaddress,
        "currency" : paymentObj.currency,
        "accessKey": paymentObj.accessKey,
        "description" : paymentObj.description,
        "amount": paymentObj.amount
    })
    .end(function (response) {
        if(response.body === undefined) {
            deferred.reject(undefined)
        }
        else {
            if(response.body['statusCode'] == 200) {
                deferred.resolve(response.body)
            }
            else {
                deferred.reject(response.body)
            }
        }   
    });
    return deferred.promise;   
}

function StripePaymentRequest(paymentObj) {
    var deferred = Q.defer();
    
    unirest.post(config.paymentGateway.devEndpoint + "/stripe/payment")
    .headers({'Accept': 'application/json', 'Content-Type': 'application/json'})
    .auth({user : "user", pass: "password", sendImmediately: true})
    .send({ 
        "cardNumber": paymentObj.cardNumber, 
        "expMonth": paymentObj.cardMonth,
        "ccv" : paymentObj.ccv,
        "expYear": paymentObj.cardYear,
        "originalPaymentValue": paymentObj.originalValue,
        "customerIP" : paymentObj.ipaddress,
        "currency" : paymentObj.currency,
        "accessKey": paymentObj.accessKey,
        "description" : paymentObj.description,
        "amount": paymentObj.amount
    })
    .end(function (response) {
        if(response.body === undefined) {
            deferred.reject(undefined)
        }
        else {
            if(response.body['statusCode'] == 200) {
                deferred.resolve(response.body)
            }
            else {
                deferred.reject(response.body)
            }
        }   
    });
    return deferred.promise;   
}

function GetProvidersKeyRequest(typeParam) {
    var deferred = Q.defer();
    var client = new Client();

    var args = {
        headers: { 
            "Content-Type": "application/json",
            "Authorization" : config.API.accessKey
        },
        parameters: { 
            type: typeParam
        }
    };

    client.get(config.API.endpointdev + "/client/getproviderkey", args, function (data, response) {
        if(data['statusCode'] == 200) {
            deferred.resolve(data)
        }
        else {
            deferred.reject(data)
        }   
    });
    return deferred.promise;   
}

function PreBuyRequest(productsArr) {
    var deferred = Q.defer();
    var client = new Client();

    var args = {
        headers: { 
            "Content-Type": "application/json",
            "Authorization" : config.API.accessKey
        },
        data: { 
            products: productsArr
        }
    };
    client.patch(config.API.endpointdev + "/client/prebuy", args, function (data, response) {
        
        if(data['statusCode'] == 200) {
            deferred.resolve(data)
        }
        else {
            deferred.reject(data)
        }   
    });
    return deferred.promise;   
}

function ReversePreBuyRequest(productsArr) {
    var deferred = Q.defer();
    var client = new Client();

    var args = {
        headers: { 
            "Content-Type": "application/json",
            "Authorization" : config.API.accessKey
        },
        data: { 
            products: productsArr
        }
    };

    client.patch(config.API.endpointdev + "/client/reverseprebuy", args, function (data, response) {
        if(data['statusCode'] == 200) {
            deferred.resolve(data)
        }
        else {
            deferred.reject(data)
        }   
    });
    return deferred.promise;   
}

function SavePurchaseRequest(newPurchaseObject) {
    var deferred = Q.defer();
    var client = new Client();

    var args = {
        headers: { 
            "Content-Type": "application/json",
            "Authorization" : config.API.accessKey
        },
        data: { 
            paymentID: newPurchaseObject.paymentID,
            paymentTokenID: newPurchaseObject.paymentTokenID,
            products: newPurchaseObject.products,
            paymentProcessedDate: newPurchaseObject.paymentProcessedDate,
            clientinfo: newPurchaseObject.clientinfo,
            total: newPurchaseObject.total,
            currency: newPurchaseObject.currency,
            otherCurrencyTotal: newPurchaseObject.otherTotal,
            paymentprovider: newPurchaseObject.paymentprovider,
            deliveryinfo: newPurchaseObject.deliveryinfo
        }
    };

    client.patch(config.API.endpointdev + "/client/buy", args, function (data, response) {
        if(data['statusCode'] == 200) {
            deferred.resolve(data)
        }
        else {
            deferred.reject(data)
        }   
    });
    return deferred.promise;   
}

module.exports = {
    ProcessEkumbuPayment: function(request, reply) {

        var cardNumberParam = request.payload.cardN;
        var cardExpdate = request.payload.expDate;

        var cardMonthParam = cardExpdate.substring(0,1);
        var cardYearParam = cardExpdate.substring(2, cardExpdate.length);

        GetProvidersKeyRequest('ekumbu')
        .then(ProdiversKeyFound => {
            var paymentObj = {
                cardNumber: cardNumberParam,
                cardMonth: cardMonthParam,
                cardYear: cardYearParam,
                amount : request.session.subtotal,
                accessKey: ProdiversKeyFound.key,
                currency: 'AOA',
                description: 'payment for product X',
                ipaddress: request.info.remoteAddress
            }
            
            PreBuyRequest(request.session.cart)
            .then(PreBuyResponse => {
                if(isEmptyObject(PreBuyResponse.notavailable)) {
                    EkumubPaymentRequest(paymentObj)
                    .then(PayResponse => {
                        //Send Save Purchase Request

                        var NewObjPurchase = {
                            paymentID: PayResponse.paymentid,
                            paymentTokenID: PayResponse.paymentTokenID,
                            paymentProcessedDate: PayResponse.dateProcessed,
                            currency: 'AOA',
                            otherTotal: request.session.subtotal,
                            products: request.session.cart,
                            clientinfo: {
                                name: request.payload.customerName,
                                phone: request.payload.customerPhone,
                                email: request.payload.customerEmail
                            },
                            total: request.session.subtotal,
                            paymentprovider: "ekumbu",
                            deliveryinfo: {
                                deliverable: true,
                                name: request.payload.customerName,
                                phone: request.payload.customerPhone,
                                address: {
                                    street: request.payload.addressStreet,
                                    city: request.payload.addressCity,
                                    country: request.payload.addressCountry
                                }
                            }
                        }
                        SavePurchaseRequest(NewObjPurchase)
                        .then(PurchaseDone => {
                            console.log(PurchaseDone)
                            request.session.paymentdone = 'done'; //move this to payment sucessful 
                            request.session.paymentID = PurchaseDone.purchaseID
                            request.session.cart = [];
                            var infResp = {
                                statusCode: 200,
                                error: null,
                                message: 'successpurchase'
                            }
                            return reply(infResp)
                        })
                        .catch(error => {
                            console.log('purchase error - ' + error)
                            var infResp = {
                                statusCode: 403,
                                error: error.error,
                                message: error.message
                            }
                            return reply(infResp)
                        })
                    })
                    .catch(paymentError => {
                        ReversePreBuyRequest(PreBuyResponse.available)
                        .then(PreSellReversed => {
                            var infResp = {
                                statusCode: 403,
                                error: paymentError.error,
                                message: paymentError.message
                            }
                            return reply(infResp)
                        })
                    })   
                }
                else {
                    /**
                     * TODO 
                     * improve the language and logic because it can be only one product
                     */
                    ReversePreBuyRequest(PreBuyResponse.available)
                    .then(PreSellReversed => {
                        var infResp = {
                            statusCode: 403,
                            error: 'Request Error',
                            message: 'some products are no longer availables',
                            products: PreBuyResponse.notavailable
                        }
                        return reply(infResp) 
                    })
                    
                }
            })
            .catch(error => {
                console.log(error)
                return reply(error)
            })
            
        })
        .catch(error => {
            console.log(error);
            var infResp = {
                statusCode: 500,
                error: 'Internal Error',
                message: 'an internal error occured while processing payment. contact the administrator'
            }
            return reply(infResp)
        })
    },

    AuthorizeEkumbuPayment: function(request, reply) {

    },

    ProcessStripePayment: function(request, reply) {
        
        var cardNumberParam = request.payload.cardN;
        var cardExpdate = request.payload.expDate;

        var cardMonthParam = cardExpdate.substring(0,2);
        var cardYearParam = cardExpdate.substring(3, cardExpdate.length);

        GetProvidersKeyRequest('stripe')
        .then(ProdiversKeyFound => {
            GetRate('USD')
            .then(FoundRate => {
                var paymentObj = {
                    cardNumber: cardNumberParam,
                    cardMonth: cardMonthParam,
                    cardYear: cardYearParam,
                    ccv: request.payload.ccv,
                    originalValue: (request.session.subtotal/FoundRate.rate).toFixed(2),
                    amount : 100 * (request.session.subtotal/FoundRate.rate).toFixed(2),
                    accessKey: ProdiversKeyFound.key,
                    currency: 'usd',
                    description: 'payment for product X',
                    ipaddress: request.info.remoteAddress
                }
                
                PreBuyRequest(request.session.cart)
                .then(PreBuyResponse => {
                    if(isEmptyObject(PreBuyResponse.notavailable)) {
                        StripePaymentRequest(paymentObj)
                        .then(PayResponse => {
                            //Send Save Purchase Request
                            var NewObjPurchase = {
                                paymentID: PayResponse.paymentid,
                                paymentTokenID: PayResponse.paymentTokenID,
                                paymentProcessedDate: PayResponse.dateProcessed,
                                currency: 'usd',
                                otherTotal: (request.session.subtotal / FoundRate.rate).toFixed(2),
                                products: request.session.cart,
                                clientinfo: {
                                    name: request.payload.customerName,
                                    phone: request.payload.customerPhone,
                                    email: request.payload.customerEmail
                                },
                                total: request.session.subtotal,
                                paymentprovider: "stripe",
                                deliveryinfo: {
                                    deliverable: true,
                                    name: request.payload.customerName,
                                    phone: request.payload.customerPhone,
                                    address: {
                                        street: request.payload.addressStreet,
                                        city: request.payload.addressCity,
                                        country: request.payload.addressCountry
                                    }
                                }
                            }
                            SavePurchaseRequest(NewObjPurchase)
                            .then(PurchaseDone => {
                                console.log(PurchaseDone)
                                request.session.paymentdone = 'done'; //move this to payment sucessful 
                                request.session.paymentID = PurchaseDone.purchaseID
                                request.session.cart = [];
                                var infResp = {
                                    statusCode: 200,
                                    error: null,
                                    message: 'successpurchase'
                                }
                                return reply(infResp)
                            })
                            .catch(error => {
                                console.log('purchase error - ' + error)
                                var infResp = {
                                    statusCode: 403,
                                    error: error.error,
                                    message: error.message
                                }
                                return reply(infResp)
                            })
                            

                        })
                        .catch(paymentError => {
                            ReversePreBuyRequest(PreBuyResponse.available)
                            .then(PreSellReversed => {
                                var infResp = {
                                    statusCode: 403,
                                    error: paymentError.error,
                                    message: paymentError.message
                                }
                                return reply(infResp)
                            })
                        })   
                    }
                    else {
                        /**
                         * TODO 
                         * improve the language and logic because it can be only one product
                         */
                        ReversePreBuyRequest(PreBuyResponse.available)
                        .then(PreSellReversed => {
                            var infResp = {
                                statusCode: 403,
                                error: 'Request Error',
                                message: 'some products are no longer availables',
                                products: PreBuyResponse.notavailable
                            }
                            return reply(infResp) 
                        })
                        
                    }
                })
                .catch(error => {
                    console.log(error)
                    return reply(error)
                })
            })
        })
        .catch(error => {
            console.log(error);
            var infResp = {
                statusCode: 500,
                error: 'Internal Error',
                message: 'an internal error occured while processing payment. contact the administrator'
            }
            return reply(infResp)
        })
    },
    
}