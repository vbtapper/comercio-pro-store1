"use strict";
$(document).ready(function(){
    $("#paymentContent").css("visibility", "hidden");
    $('body').on('click', '#btnEkumbuPay', function(){
        $.ajax({
            type: "POST",
            url: "/payment/ekumbu",
            data: {cardN : $("#txtCardNumber").val(), expDate: $("#txtExpDate").val(), customerName: $("#customerName").val(), customerPhone: $("#customerPhone").val(), customerEmail: $("#customerEmail").val(), addressStreet: $("#addressStreet").val(), addressCity: $("#addressCity").val(), addressCountry: $("#addressCountry").val() },
            async: false,
            success: function(response) { 
                if(response['statusCode'] === 200) {
                    window.location.href = response['message']; 
                }
                else {
                    alert(response['message']);
                }
            },
        });
    })

    $("#paymentContent").css("visibility", "hidden");
    $('body').on('click', '#btnVisaPay', function(){
        $.ajax({
            type: "POST",
            url: "/payment/visa",
            data: {cardN : $("#txtCardNumber").val(), expDate: $("#txtExpDate").val(), ccv: $("#txtCcv").val(), customerName: $("#customerName").val(), customerPhone: $("#customerPhone").val(), customerEmail: $("#customerEmail").val(), addressStreet: $("#addressStreet").val(), addressCity: $("#addressCity").val(), addressCountry: $("#addressCountry").val() },
            async: false,
            success: function(response) { 
                if(response['statusCode'] === 200) {
                    window.location.href = response['message']; 
                }
                else {
                    alert(response['message']);
                }
            },
        });
    })
});

function ChoosePaymentType(typeParam) {
    
    $.ajax({
        type: "POST",
        url: "/choosepaymenttype",
        data: {type : typeParam},
        async: false,
        success: function(response) { 
            if(response['statusCode'] === 200) {
                if(typeParam === "ekumbu") {
                    $("#paymentContent").empty();
                    $("#paymentContent").css("visibility", "visible");
                    var cardNumber = $("<input>", {"class": "paymentNumber", "type" : "text", "id" : "txtCardNumber", "placeholder" : "Número do Cartão"});
                    var expDate = $("<input>", {"class": "paymentExp", "type" : "text", "id" : "txtExpDate", "placeholder" : "Data de Válidade"});
                    var btnPay = $("<button>", {"id" : "btnEkumbuPay", "text": "Efectuar Pagamento", "class" : "checkoutButton"});
                    $("#paymentContent").append(cardNumber);
                    $("#paymentContent").append(expDate);
                    $("#paymentContent").append(btnPay);
                }
                else {
                    $("#paymentContent").empty();
                    $("#paymentContent").css("visibility", "visible");
                    var cardNumber = $("<input>", {"class": "paymentNumber", "type" : "text", "id" : "txtCardNumber", "placeholder" : "Card Number"});
                    var expDate = $("<input>", {"class": "paymentExp", "type" : "text", "id" : "txtExpDate", "placeholder" : "Expiration Date"});
                    var expCCv = $("<input>", {"class": "paymentExp", "type" : "text", "id" : "txtCcv", "placeholder" : "CCV"});
                    var btnPay = $("<button>", {"id" : "btnVisaPay", "text": "Make Payment", "class" : "checkoutButton"});
                    $("#paymentContent").append(cardNumber);
                    $("#paymentContent").append(expDate);
                    $("#paymentContent").append(expCCv);
                    $("#paymentContent").append(btnPay);
                }
            }
            else {
                alert(response['message']);
            }
        },
    });
}