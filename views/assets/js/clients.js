"use strict";
$(document).ready(function(){
    $('body').on('click', '#btnJoinSend', function(){
        $.ajax({
            type: "POST",
            url: "/join/register",
            data: {firstname: $("#txtJoinFirstName").val(), lastname: $("#txtJoinLastName").val(), email: $("#txtJoinEmail").val(), password: $("#txtJoinPassword").val()},
            async: false,
            success: function(response) { 
                if(response['statusCode'] === 200) {
                    window.location.href = response['message'];   
                }
                else {
                    alert(response['message'])
                }
            },
        });
    })

    $('body').on('click', '#btnLogin', function(){
        
        $.ajax({
            type: "POST",
            url: "/login/action",
            data: {email: $("#txtLoginEmail").val(), password: $("#txtLoginPassword").val()},
            async: false,
            success: function(response) { 
                if(response['statusCode'] === 200) {
                    window.location.href = response['message'];   
                }
                else {
                    alert(response['message'])
                }
            },
        });
    })

    $('body').on('click', '#cmdShowAddress', function(){
        $("#frmNewAddress").css('visibility', 'visible');
    })
  
    $('body').on('click', '#cmdAddNewAddress', function(){
        $.ajax({
            type: "POST",
            url: "/profile/addclientaddress",
            data: {street: $("#txtStreet").val(), city: $("#txtCity").val(), country: $("#txtCountry").val()},
            async: false,
            success: function(response) { 
                if(response['statusCode'] === 200) {
                    window.location.href = response['message'];   
                }
                else {
                    alert(response['message'])
                }
            },
        });
    })

})