"use strict";
var productReviewRate = -1;

$(document).ready(function(){
    $('body').on('click', '#btnLoadMore', function(){
        $.ajax({
            type: "GET",
            url: "products/increaselimit",
            async: false,
            success: function(response) { 
                if(response['statusCode'] === 200) {
                    window.location.href = response['message'];   
                }
            },
        });
    })

    $('body').on('click', '#btnAddToCart', function(){
        $.ajax({
            type: "POST",
            url: "/products/addtocart",
            data: {quantity: $("#dllProductQuantity").val()},
            async: false,
            success: function(response) { 
                if(response['statusCode'] === 200) {
                    var msg = response['cartmessage']
                    $("#mainCartMessage").html(msg)
                    $("#mainCartCounter").text("("+response['nitens']+")")
                    $("#cart").slideDown("slow");
                }
                else {
                    alert(response['message'])
                }
            },
        });
    })
});

function ProductSelectColor(colorParam, objSelected, colorValueParam) {
    $.ajax({
        type: "POST",
        url: "/products/selectcolor",
        data: {colorID: colorParam, colorValue: colorValueParam},
        async: false,
        success: function(response) { 
            if(response['statusCode'] === 200) {
                var ulSizes = $("#productsUlSizes");
                
                ulSizes.empty();
                ulSizes.append("<h3>available Sizes </h3>");
                response['array'].forEach(function(element) {
                    ulSizes.append("<li id='"+element.id+"' onclick='ProductSelectSize(\""+element.id+"\","+element.value+")' class='noSelectedColor'><a><span>"+element.value+"</span></a></li>")
                });

                $('#productUlColors li').each(function(){
                    this.classList.remove('selectedColor');
                    this.classList.add('noSelectedColor');
                });

                objSelected.classList.remove('noSelectedColor');
                objSelected.classList.add('selectedColor');
            }
            else {
                alert(response['message']);
            }
        },
    });
}

function ProductSelectSize(sizeParam, sizeValueParam) {
    $.ajax({
        type: "POST",
        url: "/products/selectsize",
        data: {sizeID: sizeParam, sizeValue: sizeValueParam},
        async: false,
        success: function(response) { 
            if(response['statusCode'] === 200) {

                $('#productsUlSizes li').each(function(){
                    this.classList.remove('selectedColor');
                    this.classList.add('noSelectedColor');
                });

                var objSelected = document.getElementById(sizeParam);

                objSelected.classList.remove('noSelectedColor');
                objSelected.classList.add('selectedColor');
            }
            else {
                alert(response['message']);
            }
        },
    });
}

function RemoveFromCart(productIDParam) {
    swal({
            title: 'Remover do Carrinho',
            text: "Deseja remover o item do carrinho?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            cancelButtonText: 'Cancelar',
            confirmButtonText: 'Sim, Remova!'
        })
        .then(function () {
            $.ajax({
            type: "POST",
                url: "cart/remove",
                data: {productID: productIDParam},
                async: false,
                success: function(response) { 
                    if(response['statusCode'] === 200) {
                        window.location.href = response['message'];   
                    }
                },
            });
        }
    )
}

function DecreaseQuantity(productIDParam) {
    $.ajax({
    type: "POST",
        url: "cart/descreaseQt",
        data: {productID: productIDParam},
        async: false,
        success: function(response) { 
            if(response['statusCode'] === 200) {
                window.location.href = response['message'];   
            }
        },
    });
}

function IncreaseQuantity(productIDParam) {
    $.ajax({
    type: "POST",
        url: "cart/increaseQt",
        data: {productID: productIDParam},
        async: false,
        success: function(response) { 
            if(response['statusCode'] === 200) {
                window.location.href = response['message'];   
            }
        },
    });
}

function EnableCheckout() {
    $.ajax({
    type: "POST",
        url: "cart/enablecheckout",
        async: false,
        success: function(response) { 
            if(response['statusCode'] === 200) {
                window.location.href = response['message'];   
            }
        },
    });
}

function SetProductReviewRate(rate) {
    productReviewRate = rate
}