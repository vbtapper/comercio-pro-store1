const formatCurrency        = require('format-currency')
const Client                = require('node-rest-client').Client;
const Q                     = require('q');

const config                = require('../../config.js');

module.exports = function(value, locale, rate) {
    let opts = null;

    if(locale === 'en') {
        opts = { format: '%s %v', symbol: '$' }
        return formatCurrency((value / rate).toString(), opts);
    }
    else {
        opts = {  format: '%c %v', code: 'kz' };
        return formatCurrency(value, opts);
    }
   
}